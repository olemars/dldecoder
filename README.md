# Darklands data file decoder
_Migrated from repository on my old bitbucket account_

_Original commit was in November 2010_

Someone wanted to inspect the sprite files in the 1992 MPS game Darklands and noone had been able to reverse engineer the image format. Seemed like a good research exercise both for debugging old software and programming with the model/view pattern, so the challenge was accepted.

This small program can read the original archive files (which are in their own custom format), identify and extract the various datafiles they contain and convert the character sprites and palette data (also in their own custom, homebrew format) to PNG. You can also browse the contents and view the sprites directly.
I started looking at decoding the map files as well, but ran out of time and motivation before that was complete. PIC and PAN files (images and audio) are also not decoded.

The data files are not included and must be obtained from an original copy of the game, available for purchase on gog.com and steam.

