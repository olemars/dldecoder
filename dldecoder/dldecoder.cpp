#include "dldecoder.h"

#include <QFileDialog>
#include <QFileInfo>
#include <QString>
#include <QPixmap>
#include <QMenu>
#include <QAction>

#include "datamodel.h"
#include "colortable.h"

#include "catalog.h"
#include "binfile.h"
#include "imcfile.h"
#include "compressedfile.h"
#include "sprite.h"

DLDecoder::DLDecoder(QWidget *parent, Qt::WindowFlags flags)
:	QMainWindow(parent, flags)
,	mFileContextMenu(NULL)
,	mExportAction(NULL)
,	mSaveAction(NULL)

{
	ui.setupUi(this);
	mModel = new DataModel(this);
	ui.treeView->setModel(mModel);
	ui.treeView->setContextMenuPolicy(Qt::CustomContextMenu);

	setupContextMenu();
	setupPaletteSelector();

	connect(ui.actionExit, SIGNAL(triggered(bool)), this, SLOT(close()));
	connect(ui.actionOpen, SIGNAL(triggered(bool)), this, SLOT(open()));
	connect(ui.treeView->selectionModel(), SIGNAL(currentChanged(const QModelIndex &, const QModelIndex &)), this, SLOT(onSelection(const QModelIndex &, const QModelIndex &)));
	connect(ui.treeView, SIGNAL(customContextMenuRequested(const QPoint &)), this, SLOT(onContextMenuRequested(const QPoint &)));
}

DLDecoder::~DLDecoder()
{

}


void
DLDecoder::open()
{
	QString filter = "Catalog files(*.cat);;IMC files (*.imc);;All files (*.*)";
	QStringList files =	QFileDialog::getOpenFileNames(this, "Select Files", "", filter);

	QStringList list = files;
	foreach(QString filepath, list)
	{
		QFileInfo fileInfo(filepath);
		if(fileInfo.suffix().compare("cat", Qt::CaseInsensitive) == 0
		|| fileInfo.fileName().compare("bc", Qt::CaseInsensitive) == 0
		|| fileInfo.fileName().compare("lcastle", Qt::CaseInsensitive) == 0)
		{
			loadCatalogFile(filepath);
		}
		else if(fileInfo.suffix().compare("imc", Qt::CaseInsensitive) == 0)
		{
			loadImcFile(filepath);
		}
		else if(fileInfo.suffix().compare("pan", Qt::CaseInsensitive) == 0)
		{
			loadCompressedFile(filepath);
		}
	}
}


void
DLDecoder::loadCatalogFile(QString aFilePath)
{
	Catalog *catalog = Catalog::loadCatalogFile(aFilePath);
	if(catalog)
	{
		//catalog->setParent(mModel->getRoot());
		mModel->addFile(catalog);//mFiles.push_back(catalog);
	}
}


void
DLDecoder::loadImcFile(QString aFilePath)
{
	ImcFile *imcfile = ImcFile::loadImcFile(aFilePath);
	if(imcfile)
	{
		//imcfile->setParent(mModel->getRoot());
		mModel->addFile(imcfile);//mFiles.push_back(catalog);
	}
}

void
DLDecoder::loadCompressedFile(QString aFilePath)
{
	CompressedFile *compressedFile = CompressedFile::loadCompressedFile(aFilePath);
	if(compressedFile)
	{
		mModel->addFile(compressedFile);
	}
}


void
DLDecoder::onSelection(const QModelIndex &aCurrent, const QModelIndex &aPrev)
{
    Q_UNUSED(aPrev);
	ui.label->clear();
	ui.mBlock32Combo->setEnabled(false);
	ui.mBlock48Combo->setEnabled(false);
	ui.mBlock64Combo->setEnabled(false);

	if(aCurrent.isValid())
	{
		BinFile *selected = static_cast<BinFile *>(aCurrent.internalPointer());
		if(selected)
		{
			if(dynamic_cast<ImcFile *>(selected))
			{
				ui.mBlock32Combo->setEnabled(true);
				ui.mBlock48Combo->setEnabled(true);
				ui.mBlock64Combo->setEnabled(true);
				int block32, block48, block64;
				dynamic_cast<ImcFile *>(selected)->retrievePaletteBlocks(block32, block48, block64);
				setPaletteBlocks(block32, block48, block64);
			}
			if(dynamic_cast<Sprite *>(selected))
			{
				ui.mBlock32Combo->setEnabled(true);
				ui.mBlock48Combo->setEnabled(true);
				ui.mBlock64Combo->setEnabled(true);
				int block32, block48, block64;
				dynamic_cast<Sprite *>(selected)->retrievePaletteBlocks(block32, block48, block64);
				setPaletteBlocks(block32, block48, block64);

				QImage *image = dynamic_cast<Sprite *>(selected)->getImage();
				if(image == NULL)
					return;
				ui.label->setPixmap(QPixmap::fromImage(image->scaled(image->size() * 5)));
				//image->scaled(image->size() * 5).save(selected->getName() + ".png");
			}
		}
	}
}


void
DLDecoder::onPaletteSelection()
{
	int block32 = ui.mBlock32Combo->currentIndex();
	int block48 = ui.mBlock48Combo->currentIndex();
	int block64 = ui.mBlock64Combo->currentIndex();
	ColorTable *colors = ColorTable::sInstance();
	if(block32 > colors->getBlock32Count())
		block32 = colors->getBlock32Count() - 1;
	if(block48 > colors->getBlock48Count())
		block48 = colors->getBlock48Count() - 1;
	if(block64 > colors->getBlock64Count())
		block64 = colors->getBlock64Count() - 1;

	QModelIndex currIdx = ui.treeView->selectionModel()->currentIndex();
	if(currIdx.isValid())
	{
		BinFile *selected = static_cast<BinFile *>(currIdx.internalPointer());
		if(selected)
		{
			if(dynamic_cast<ImcFile *>(selected))
			{
				dynamic_cast<ImcFile *>(selected)->setPaletteBlocks(block32, block48, block64);
			}
			else if(dynamic_cast<Sprite *>(selected))
			{
				Sprite *sprite = dynamic_cast<Sprite *>(selected);
				if(sprite->parent() != NULL && dynamic_cast<ImcFile *>(sprite->parent()))
					dynamic_cast<ImcFile *>(sprite->parent())->setPaletteBlocks(block32, block48, block64);
				else
					sprite->setPaletteBlocks(block32, block48, block64);
			}
			onSelection(ui.treeView->selectionModel()->currentIndex(), QModelIndex());
		}
	}
}

void
DLDecoder::setPaletteBlocks(int aBlock32, int aBlock48, int aBlock64)
{
	int block32 = ui.mBlock32Combo->currentIndex();
	int block48 = ui.mBlock48Combo->currentIndex();
	int block64 = ui.mBlock64Combo->currentIndex();
	if(aBlock32 != block32 || aBlock48 != block48 || aBlock64 != block64)
	{
		ui.mBlock32Combo->setCurrentIndex(aBlock32);
		ui.mBlock48Combo->setCurrentIndex(aBlock48);
		ui.mBlock64Combo->setCurrentIndex(aBlock64);
	}

}


void
DLDecoder::setupPaletteSelector()
{
	mPaletteModel = new QStandardItemModel(this);
	ColorTable *colors = ColorTable::sInstance();
	for(int i = 0; i < colors->getBlock32Count(); i++)
		mPaletteModel->setItem(i, 0, new QStandardItem(QString("%1").arg(i)));
	for(int i = 0; i < colors->getBlock48Count(); i++)
		mPaletteModel->setItem(i, 1, new QStandardItem(QString("%1").arg(i)));
	for(int i = 0; i < colors->getBlock64Count(); i++)
		mPaletteModel->setItem(i, 2, new QStandardItem(QString("%1").arg(i)));
	ui.mBlock32Combo->setModel(mPaletteModel);
	ui.mBlock32Combo->setModelColumn(0);
	connect(ui.mBlock32Combo, SIGNAL(currentIndexChanged (int)), this, SLOT(onPaletteSelection())); 
	ui.mBlock48Combo->setModel(mPaletteModel);
	ui.mBlock48Combo->setModelColumn(1);
	connect(ui.mBlock48Combo, SIGNAL(currentIndexChanged (int)), this, SLOT(onPaletteSelection()));
	ui.mBlock64Combo->setModel(mPaletteModel);
	ui.mBlock64Combo->setModelColumn(2);
	connect(ui.mBlock64Combo, SIGNAL(currentIndexChanged (int)), this, SLOT(onPaletteSelection()));

	ui.mBlock32Combo->setEnabled(false);
	ui.mBlock48Combo->setEnabled(false);
	ui.mBlock64Combo->setEnabled(false);
}

void
DLDecoder::setupContextMenu()
{
	mFileContextMenu = new QMenu(this);

	mSaveAction = mFileContextMenu->addAction("Save single file", this, SLOT(saveFileSlot()));
	mExportAction = mFileContextMenu->addAction("Export all children", this, SLOT(exportFileSlot()));

}

void
DLDecoder::onContextMenuRequested(const QPoint &aPoint)
{
	BinFile *selected = static_cast<BinFile *>(ui.treeView->selectionModel()->currentIndex().internalPointer());
	if(selected)
	{
		mSaveAction->setEnabled(true);
		mExportAction->setEnabled(true);
		if(selected->childCount() == 0)
			mExportAction->setEnabled(false);
		if(dynamic_cast<Sprite *>(selected))
		{
			if(dynamic_cast<Sprite *>(selected)->getImage() == NULL)
				mSaveAction->setEnabled(false);
			mExportAction->setEnabled(false);
		}
		if(dynamic_cast<Catalog *>(selected))
			mSaveAction->setEnabled(false);

		mFileContextMenu->exec(ui.treeView->viewport()->mapToGlobal(aPoint));
	}
}

void
DLDecoder::saveFileSlot()
{
	BinFile *selected = static_cast<BinFile *>(ui.treeView->selectionModel()->currentIndex().internalPointer());
	if(selected)
	{
		if(dynamic_cast<Sprite *>(selected))
		{
			Sprite *sprite = dynamic_cast<Sprite *>(selected);
			QString filter = "Supported image formats(*.png *.bmp *.jpg *.gif *.tiff *.xpm)";
			QString filename =	QFileDialog::getSaveFileName(this, "Save as image file", "./" + sprite->getName() + ".png", filter);
			if(filename.isEmpty())
				return;
			sprite->save(filename);
		}
		else
		{
			QString filename =	QFileDialog::getSaveFileName(this, "Save as", "./" + selected->getName());
			if(filename.isEmpty())
				return;
			selected->save(filename);
		}

	}
}


void
DLDecoder::exportFileSlot()
{
	BinFile *selected = static_cast<BinFile *>(ui.treeView->selectionModel()->currentIndex().internalPointer());
	if(selected)
	{
		QString destdir =	QFileDialog::getExistingDirectory(this, "Choose destination for export", ".");
		if(destdir.isEmpty())
			return;

		selected->exportChildren(destdir);

	}

}
