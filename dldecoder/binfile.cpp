#include "binfile.h"

#include <QDir>
#include <QFile>

BinFile::BinFile(const QByteArray &aRawData, QString aName, quint32 aSize, BinFile *aParent)
:	mRawData(aRawData)
,	mName(aName)
,	mSize(aSize)
,	mParent(aParent)
{

}

BinFile::~BinFile()
{
	qDeleteAll(mChildFiles);
	mChildFiles.clear();
}

void
BinFile::addChild(BinFile *aFile)
{
	if(aFile)
	{
		aFile->setParent(this);
		mChildFiles.push_back(aFile);
	}
}


QString
BinFile::getName() const
{
	return mName;
}

BinFile *
BinFile::parent()
{
	return mParent;
}

int
BinFile::row() const
{
	if(mParent)
		return mParent->childIndex(const_cast<BinFile *>(this));
	return 0;
}

BinFile *
BinFile::childFile(int aIdx)
{
	if(aIdx >= 0 && aIdx < mChildFiles.count())
		return mChildFiles.at(aIdx);
	return NULL;
}

int
BinFile::childIndex(BinFile *aFile) const
{
	return mChildFiles.indexOf(aFile);
}

int
BinFile::childCount(bool aRecursive) const
{
	int totalCount = mChildFiles.count();
	if(aRecursive)
	{
		foreach(BinFile *child, mChildFiles)
			totalCount += child->childCount();
	}

	return totalCount;
}


void
BinFile::setParent(BinFile *aParent)
{
	mParent = aParent;
}

void
BinFile::save(QString aFileName)
{
	QFile file(aFileName);
	file.open(QIODevice::WriteOnly);
	file.write(mRawData);
	file.close();
}


void
BinFile::exportChildren(QString aPath)
{
	if(mChildFiles.size() == 0)
		return;

	QDir dir(aPath + "/" + mName + ".DIR");
	if(!dir.exists())
		dir.mkpath(dir.absolutePath());


	foreach(BinFile *file, mChildFiles)
	{
		file->save(dir.absolutePath() + "/" + file->getName());
		file->exportChildren(dir.absolutePath());
	}
}