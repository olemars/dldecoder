#ifndef COMPRESSEDFILE_H
#define COMPRESSEDFILE_H

#include "binfile.h"

#include <QByteArray>
#include <QBuffer>


/** This class is used for files using the same decompression technique as IMC files, but where the contents won't currently be further processed.
*/
class CompressedFile : public BinFile
{
public:
	CompressedFile(const QByteArray &aRawData, QString aName, quint32 aSize, BinFile *aParent = NULL);
	~CompressedFile();

	static CompressedFile *loadCompressedFile(QString aFilePath);

	void save(QString aFileName);

private:

	QByteArray mUncompressedData;
	
};

#endif // COMPRESSEDFILE_H
