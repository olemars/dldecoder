#ifndef BINFILE_H
#define BINFILE_H

#include <QByteArray>
#include <QString>
#include <QList>


/** Generic class for a file with non-descript binary data. All imported file types in this application should inherit from this class. Contains various access/utility functions, some used by the data model, some for file operations. Most subclasses will want to reimplement save() and exportChildren().
*/
class BinFile
{

public:
	BinFile(const QByteArray &aRawData, QString aName, quint32 aSize, BinFile *aParent = NULL);
	virtual ~BinFile();

	virtual BinFile *parent();
    virtual int row() const;

	virtual void addChild(BinFile *aFile);
	virtual BinFile *childFile(int aIdx);
	virtual int childIndex(BinFile *aFile) const;
	virtual int childCount(bool aRecursive = false) const;
	virtual QString getName() const;
	void setParent(BinFile *aParent);

	virtual void save(QString aFileName);
	virtual void exportChildren(QString aPath);

protected:
	quint32 mSize;
	QString mName;
	QByteArray mRawData;
	BinFile *mParent;
	QList<BinFile *> mChildFiles;
};

#endif // BINFILE_H
