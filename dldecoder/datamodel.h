#ifndef DATAMODEL_H
#define DATAMODEL_H

#include <QObject>
#include <QVariant>
#include <QAbstractItemModel> 
#include <QModelIndex>

#include "binfile.h"

/** Model in a model/view setup for the treeview in the gui listing the contents of BinFiles
*/
class DataModel : public QAbstractItemModel
{
	Q_OBJECT

public:
	DataModel(QObject *aParent = 0);
	~DataModel();

	QVariant data(const QModelIndex &index, int role) const;
	Qt::ItemFlags flags(const QModelIndex &index) const;
	QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const;
	QModelIndex index(int row, int column, const QModelIndex &parent = QModelIndex()) const;
	QModelIndex parent(const QModelIndex &index) const;
    int rowCount(const QModelIndex &parent = QModelIndex()) const;
    int columnCount(const QModelIndex &parent = QModelIndex()) const { Q_UNUSED(parent); return 1; }
	bool hasChildren(const QModelIndex &parent = QModelIndex()) const;

	//void setCanFetchMore(int aCount);
	void addFile(BinFile *aFile);
	BinFile *getRoot();

protected:
	//bool canFetchMore (const QModelIndex &parent) const;
	//void fetchMore (const QModelIndex &parent);

private:
	BinFile *mRoot;

	//int mFetchCount;
	
};

#endif // DATAMODEL_H
