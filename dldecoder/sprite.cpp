#include "sprite.h"

#include <QBuffer>
#include <QDataStream>

#include "colortable.h"


Sprite::Sprite(const QByteArray &aRawData, QString aName, quint32 aSize, BinFile *aParent)
:	BinFile(aRawData, aName, aSize, aParent)
,	mImage(NULL)
,	mBlock32(0)
,	mBlock48(0)
,	mBlock64(0)
{
	decodeImage();
}

Sprite::~Sprite()
{
	delete mImage;
}


void
Sprite::decodeImage()
{
	if(mImage)
	{
		delete mImage;
		mImage = 0;
	}

	if(mRawData.size() == 0)
		return;

	//player characters use special palette blocks
	int slot = 0;
	if(mName.contains("F60"))
		slot = 1;
	if(mName.contains("F01"))
		slot = 2;
	if(mName.contains("A00"))
		slot = 3;
	if(mName.contains("C00"))
		slot = 4;

	QBuffer buffer(&mRawData);
	buffer.open(QIODevice::ReadOnly);
	QDataStream stream(&buffer);
	stream.setByteOrder(QDataStream::LittleEndian);

	//first two bytes is the width and height
	quint8 width, height;
	stream >> width;
	stream >> height;

	mImage = new QImage(width, height, QImage::Format_Indexed8);

	mImage->setColorTable(ColorTable::sInstance()->colorTable(mBlock32, mBlock48, mBlock64));


	int y = 0;
	while(y < height)
	{
		quint8 count = 0, pos = 0;
		//first byte in each row is the number of non-blank pixels in this row
		stream >> count;
		//second byte is first pixel in the row that is non-blank
		stream >> pos;

		//pad the start of the row, up to _pos_ with transparent pixels
		for(int i = 0; i < pos; i++)
		{
			mImage->setPixel(i, y, 0);
		}
		//draw the non-blank pixels starting at pos and ending at pos+count-1
		for(int i = 0; i < count; i++)
		{
			quint8 index = 0;
			stream >> index;

			index = remapIndex(index, slot);

			mImage->setPixel(pos + i, y, index);
		}
		//pad the remainder of the row with transparent pixels
		for(int i = pos + count; i < width; i++)
		{
			mImage->setPixel(i, y, 0);
		}

		y++; //row complete
	}
	//this will set all pixels with index 0 to be have alpha = 0, and add an alpha channel to the image.
	//QImage alpha = mImage->createMaskFromColor(color.colorTable().at(0), Qt::MaskOutColor);
	//mImage->setAlphaChannel(alpha);

	buffer.close();
}


QImage *
Sprite::getImage() const
{
	return mImage;
}


void
Sprite::save(QString aFileName)
{
	if(mImage)
		mImage->save(aFileName);
}


quint8
Sprite::remapIndex(quint8 aIdx, int aSlot)
{
	if(aIdx >= 235 && aIdx <= 242)
	{
		if(aSlot == 0)
			return aIdx;
		else if(aSlot == 1)
			return aIdx - 155;
		else if(aSlot == 2)
			return aIdx - 147;
		else if(aSlot == 3)
			return aIdx - 139;
		else if(aSlot == 4)
			return aIdx - 131;
	}
	
	return aIdx;
}

void
Sprite::setPaletteBlocks(int aBlock32, int aBlock48, int aBlock64)
{
	if(aBlock32 != mBlock32 || aBlock48 != mBlock48 || aBlock64 != mBlock64)
	{
		mBlock32 = aBlock32;
		mBlock48 = aBlock48;
		mBlock64 = aBlock64;
		if(mImage)
			mImage->setColorTable(ColorTable::sInstance()->colorTable(mBlock32, mBlock48, mBlock64));
	}
}

void
Sprite::retrievePaletteBlocks(int &rBlock32, int &rBlock48, int &rBlock64)
{
	rBlock32 = mBlock32;
	rBlock48 = mBlock48;
	rBlock64 = mBlock64;
}