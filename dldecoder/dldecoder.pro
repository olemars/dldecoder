#-------------------------------------------------
#
# Project created by QtCreator 2015-02-27T23:41:06
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

DESTDIR = ../bin
CONFIG(debug, debug|release) {
    TARGET = dldecoderd
}
CONFIG(release, debug|release) {
    TARGET = dldecoder
}

TEMPLATE = app

RESOURCES += \
    dldecoder.qrc

FORMS += \
    dldecoder.ui

HEADERS += \
    binfile.h \
    catalog.h \
    colortable.h \
    compressedfile.h \
    datamodel.h \
    dldecoder.h \
    filehelper.h \
    imcfile.h \
    sprite.h

SOURCES += \
    binfile.cpp \
    catalog.cpp \
    colortable.cpp \
    compressedfile.cpp \
    datamodel.cpp \
    dldecoder.cpp \
    filehelper.cpp \
    imcfile.cpp \
    main.cpp \
    sprite.cpp
