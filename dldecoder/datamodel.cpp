#include "datamodel.h"

#include "catalog.h"
#include "imcfile.h"
#include "sprite.h"


DataModel::DataModel(QObject *aParent)
:	QAbstractItemModel(aParent)
//,	mFetchCount(0)
,	mRoot(NULL)
{
	QByteArray dummy;
	mRoot = new BinFile(dummy, "Files", 0, 0);
}

DataModel::~DataModel()
{
	delete mRoot;
	mRoot = 0;
}

Qt::ItemFlags 
DataModel::flags(const QModelIndex &index) const
{
	if (!index.isValid())
		return 0;

	return Qt::ItemIsEnabled | Qt::ItemIsSelectable;
}

QVariant 
DataModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    Q_UNUSED(section);
	if (orientation == Qt::Horizontal && role == Qt::DisplayRole)
		return mRoot->getName();

	return QVariant();
}


QVariant 
DataModel::data(const QModelIndex &index, int role) const
{
	if (!index.isValid())
		return QVariant();

	if (role != Qt::DisplayRole)
		return QVariant();

    BinFile *file = static_cast<BinFile *>(index.internalPointer());

	return file->getName();
}

QModelIndex 
DataModel::index(int row, int column, const QModelIndex &parent) const
{
	if (parent.isValid() && parent.column() != 0)
		return QModelIndex();
	//if (!hasIndex(row, column, parent))
	//	return QModelIndex();

	BinFile *parentFile;

	if (!parent.isValid())
		parentFile = mRoot;
	else
		parentFile = static_cast<BinFile *>(parent.internalPointer());

	BinFile *childFile = parentFile->childFile(row);
	if (childFile)
		return createIndex(row, column, childFile);
	else
		return QModelIndex();
}


QModelIndex 
DataModel::parent(const QModelIndex &index) const
{
	if (!index.isValid())
		return QModelIndex();

	BinFile *childFile = static_cast<BinFile *>(index.internalPointer());
	BinFile *parentFile = childFile->parent();

	if (parentFile == mRoot)
		return QModelIndex();

	return createIndex(parentFile->row(), 0, parentFile);
}

int 
DataModel::rowCount(const QModelIndex &parent) const
{
	BinFile *parentFile;
	if (parent.column() > 0)
		return 0;

	if (!parent.isValid())
		parentFile = mRoot;
	else
		parentFile = static_cast<BinFile *>(parent.internalPointer());

	return parentFile->childCount();
}


bool 
DataModel::hasChildren(const QModelIndex &parent) const
{
	BinFile *parentFile;
	if (parent.column() > 0)
		return 0;

	if (!parent.isValid())
		parentFile = mRoot;
	else
		parentFile = static_cast<BinFile *>(parent.internalPointer());

	return parentFile->childCount() > 0;

}

/*
bool 
DataModel::canFetchMore(const QModelIndex &parent) const
{
	return mFetchCount > 0;
}

void 
DataModel::fetchMore(const QModelIndex &parent)
{
	
    beginInsertRows(QModelIndex(), rowCount(), rowCount() + mFetchCount);
	mFetchCount = 0;
    endInsertRows();
}
*/

void
DataModel::addFile(BinFile *aFile)
{	
	beginResetModel();
	beginInsertRows(QModelIndex(), rowCount(), rowCount());
	mRoot->addChild(aFile);
    endInsertRows();	
	//setCanFetchMore(aFile->childCount());
	endResetModel();
}

/*
void
DataModel::setCanFetchMore(int aCount)
{
	if(aCount > 0)
		mFetchCount = aCount;
	else 
		mFetchCount = 0;
}
*/

BinFile *
DataModel::getRoot()
{
	return mRoot;
}
