#ifndef DLDECODER_H
#define DLDECODER_H

#include <QList>
#include <QtWidgets/QMainWindow>
#include <QMenu>
#include <QStandardItemModel>
#include <QPoint>
#include "ui_dldecoder.h"

class DataModel;
class Catalog;
class BinFile;
class ImcFile;

/** Program class.
*/
class DLDecoder : public QMainWindow
{
	Q_OBJECT

public:
    DLDecoder(QWidget *parent = 0, Qt::WindowFlags flags = 0);
	~DLDecoder();

private slots:
	void open();
	void saveFileSlot();
	void exportFileSlot();
	void onContextMenuRequested(const QPoint &aPoint);
	void onSelection(const QModelIndex &aCurrent, const QModelIndex &aPrev);
	void onPaletteSelection();

private:
	void setupContextMenu();
	void setupPaletteSelector();
	void setPaletteBlocks(int aBlock32, int aBlock48, int aBlock64);

	void loadCatalogFile(QString aFilePath);
	void loadImcFile(QString aFilePath);
	void loadCompressedFile(QString aFilePath);
	Ui::DLDecoderClass ui;

	QMenu * mFileContextMenu;
	QAction *mExportAction;
	QAction *mSaveAction;
	QStandardItemModel *mPaletteModel;

	//QList<BinFile *> mFiles;
	DataModel *mModel;
};

#endif // DLDECODER_H
