#include "compressedfile.h"

#include <QFile>
#include <QFileInfo>
#include "filehelper.h"

CompressedFile::CompressedFile(const QByteArray &aRawData, QString aName, quint32 aSize, BinFile *aParent)
:	BinFile(aRawData, aName, aSize, aParent)
{
	FileHelper helper;
	helper.decompress(mRawData, mUncompressedData);

}

CompressedFile::~CompressedFile()
{

}


void
CompressedFile::save(QString aFileName)
{
	QFile file(aFileName);
	file.open(QIODevice::WriteOnly);
	file.write(mUncompressedData);
	file.close();
}

/** static function for loading an imc file from disk. Verifies file format(sort of), reads file and returns an ImcFile object, or NULL if file is invalid
*/
CompressedFile *
CompressedFile::loadCompressedFile(QString aFilePath)
{
	QFileInfo info(aFilePath);
	QFile file(aFilePath);

	if(!info.exists())
		return NULL;
	
	if(!file.open(QIODevice::ReadOnly))
		return NULL;

	QByteArray data = file.readAll();
	if(data.size() == 0)
		return NULL;

	//imc files don't have any format identifier, the closest thing we get is that they all have a specific EOF marker, 0xf0 0x00
	QByteArray tail;
	tail.push_back((char)0xf0);
	tail.push_back((char)0x00);
	if(!data.endsWith(tail)) 
		return NULL;

	CompressedFile *compressedFile = new CompressedFile(data, info.fileName(), data.size());
	file.close();
	return compressedFile;
}