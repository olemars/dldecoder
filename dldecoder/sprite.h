#ifndef SPRITE_H
#define SPRITE_H

#include <QImage>

#include "binfile.h"


/** A single sprite extracted from an IMC file. Original data is stored as well as a portable QImage.
*/
class Sprite : public BinFile
{
public:
	Sprite(const QByteArray &aRawData, QString aName, quint32 aSize, BinFile *aParent = NULL);
	~Sprite();

	void decodeImage();
	QImage *getImage() const;

	void setPaletteBlocks(int aBlock32, int aBlock48, int aBlock64);
	void retrievePaletteBlocks(int &rBlock32, int &rBlock48, int &rBlock64);

	void save(QString aFileName);
    void exportChildren(QString aPath) {Q_UNUSED(aPath); return;}

private:
	quint8 remapIndex(quint8 aIdx, int aSlot);
	QImage *mImage;

	int mBlock32;
	int mBlock48;
	int mBlock64;
	
};

#endif // SPRITE_H
